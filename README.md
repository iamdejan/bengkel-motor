# Bengkel Motor

Referensi alat dan bahan yang harus dibeli untuk buka bengkel motor.

## Daftar Isi

- [Alat](#alat)
  * [Kunci](#kunci)
  * [Mesin](#mesin)
    + [Darurat](#darurat)
  * [Lain-lain](#lain-lain)
- [Bahan dan Spare Part](#bahan-dan-spare-part)
  * [Umum](#umum)
  * [Honda Revo](#honda-revo)
    + [Oli](#oli)
    + [Busi](#busi)
  * [Yamaha FreeGo](#yamaha-freego)
    + [Oli](#oli-1)
      - [Kombinasi](#kombinasi)
      - [Oli Mesin](#oli-mesin)
      - [Oli Gardan](#oli-gardan)
  * [Suzuki GSX-R150](#suzuki-gsx-r150)
    + [Oli](#oli-2)
  * [Kawasaki ZX-25R](#kawasaki-zx-25r)
    + [Oli](#oli-3)
- [Referensi Lain](#referensi-lain)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Alat

### Kunci

- [Tool Box beserta Isinya 70 pcs - 12 Point Metric 95104A-70 SATA TOOLS](https://www.tokopedia.com/tchome/tool-box-beserta-isinya-70-pcs-12-point-metric-95104a-70-sata-tools)
- [Jonnesway 25 Pcs Dr. Socket Set 1/2" - S04H4125S](https://www.tokopedia.com/jonneswayofficial/jonnesway-25-pcs-dr-socket-set-1-2-s04h4125s)
- [Kunci roda palang ban mobil 14" TEKIRO 17x19x21x23mm MURAH](https://www.tokopedia.com/bomblackshop/kunci-roda-palang-ban-mobil-14-tekiro-17x19x21x23mm-murah)
- [Sata Kunci Ring Set 11 PCS 08023 Deep Offset Double Box Wrench 70°](https://www.tokopedia.com/belanjateknik/sata-kunci-ring-set-11-pcs-08023-deep-offset-double-box-wrench-70)
- Kunci Inggris / Adjustable Wrench:
    - [KUNCI INGGRIS 6 Inch TEKIRO - Adjustable Wrench Kunci Bago](https://www.tokopedia.com/indahjayatools/kunci-inggris-6-inch-tekiro-adjustable-wrench-kunci-bago)
    - [Tekiro Kunci Inggris 8" - Adjustable Wrench Tekiro 8 Inch](https://www.tokopedia.com/belanjateknik/tekiro-kunci-inggris-8-adjustable-wrench-tekiro-8-inchi-kunci-bago)
- [TEKIRO WHEEL NUT WRENCH 19 MM / KUNCI RODA L](https://www.tokopedia.com/tekiro-rexco/tekiro-wheel-nut-wrench-19-mm-kunci-roda-l-tools-alat-perkakas)
- [CORDLESS IMPACT WRENCH 48S By JLD Tools - Merah](https://www.tokopedia.com/jinluda/cordless-impact-wrench-48s-by-jld-tools-merah)
- [Deli Double Open End Spanner Set / Set Kunci Pas Dobel 12Pcs DL160012A](https://www.tokopedia.com/delitoolsofficial/deli-double-open-end-spanner-set-set-kunci-pas-dobel-12pcs-dl160012a)
- [TEKIRO T-TYPE SOCKET SET 3 PCS 8-10-12 MM - KUNCI T SET](https://www.tokopedia.com/tekiro-rexco/tekiro-t-type-socket-set-3-pcs-8-10-12-mm-kunci-t-set)
- [Kunci T 6PT Socket 14 mm T-Handle 47707 SATA TOOLS](https://www.tokopedia.com/satatools/kunci-t-6pt-socket-14-mm-t-handle-47707-sata-tools)
- [TEKIRO Y-TYPE SOCKET 10 - 12 - 14 MM / KUNCI SOK Y PENDEK](https://www.tokopedia.com/tekiro-rexco/tekiro-y-type-socket-10-12-14-mm-kunci-sok-y-pendek)
- [Tekiro Oil Filter Wrench Belt 9 Inch Kunci Oli Kulit Sabuk Termurah](https://www.tokopedia.com/serbacowok/tekiro-oil-filter-wrench-belt-9-inch-kunci-oli-kulit-sabuk-termurah)
- [Tekiro Engine block wrench 17x24 Kunci blok mesin tekiro 17x24mm](https://www.tokopedia.com/vsv/tekiro-engine-block-wrench-17x24-kunci-blok-mesin-tekiro-17x24mm)
- [09102 Kunci L Set 12 pcs Long Ball Point Hex Key Set(SAE) Sata Tools](https://www.tokopedia.com/satatools/09102-kunci-l-set-12-pcs-long-ball-point-hex-key-set-sae-sata-tools)
- [Sherlock Kunci L Set Bintang 9 Pcs mm (Pendek) / Kunci L Torx Set](https://www.tokopedia.com/sherlocktools/sherlock-kunci-l-set-bintang-9-pcs-mm-pendek-kunci-l-torx-set)
- [Kunci L Bintang Tekiro Lubang Set 9 pcs Kunci L Tekiro - Tanpa Lubang](https://www.tokopedia.com/tampomasteknik/kunci-l-bintang-tekiro-lubang-set-9-pcs-kunci-l-tekiro-tanpa-lubang)
- [Kunci L Bintang Tekiro Lubang Set 9 pcs Kunci L Tekiro - Kunci L Lubang](https://www.tokopedia.com/tampomasteknik/kunci-l-bintang-tekiro-lubang-set-9-pcs-kunci-l-tekiro-kunci-l-lubang)
- Toolbox Cabinet
    - [TOOL BOX 7 DRAWER TOOL KIT SET 246PCS SATA 09918 TOOLBOX CABINET](https://www.tokopedia.com/vinsteknik/tool-box-7-drawer-tool-kit-set-246pcs-sata-09918-toolbox-cabinet)
    - [ROLLER CABINET 7 DRAWER ISI 136 PCS TEKIRO /TROLLI 7 LACI](https://www.tokopedia.com/mitraintiperkakas/roller-cabinet-7-drawer-isi-136-pcs-tekiro-trolli-7-laci)

### Mesin

- [RYU MESIN KOMPRESOR 2850 RPM 2.5 HP 50 LITER 1500 WATT](https://www.tokopedia.com/fixcomart/ryu-mesin-kompresor-2850-rpm-2-5-hp-50-liter-1500-watt)
- [FOREDOM CC-30 Mesin Bor Gantung Die Grinder Tuner Korek Motor Porting](https://www.tokopedia.com/indahjayatools/foredom-cc-30-mesin-bor-gantung-die-grinder-tuner-korek-motor-porting)
- [Alat Pembersih Injeksi | Injection Cleaner | Injector Cleaner](https://www.tokopedia.com/jptechinalsupply/alat-pembersih-injeksi-injection-cleaner-injector-cleaner)
- [Fly Eagle Nitrogen Generator FS-4000 - Mesin Nitrogen FS4000](https://www.tokopedia.com/negeripertiwijay/fly-eagle-nitrogen-generator-fs-4000-mesin-nitrogen-fs4000)
- [Bosch Gerinda 4" Variable Speed GWS 900-100S](https://www.tokopedia.com/boschofficial/bosch-gerinda-4-variable-speed-gws-900-100s)
- [CNC Router 3040 5 Axis Mesin CNC PCB Milling with 2.2 KW Spindle](https://www.tokopedia.com/3dzaiku/cnc-router-3040-5-axis-mesin-cnc-pcb-milling-with-2-2-kw-spindle)
- [Mesin Bubut CO 632 C Long 1 Meter ( Bench Lathe ) MESIN BUBUT BESI CO](https://www.tokopedia.com/pancajaya/mesin-bubut-co-632-c-long-1-meter-bench-lathe-mesin-bubut-besi-co)

#### Darurat

- [RYU GASOLINE GENERATOR SET RG7800-1 / GENERATOR LISTRIK / GENSET](https://www.tokopedia.com/ryuofficial/ryu-gasoline-generator-set-rg7800-1-generator-listrik-genset)

### Lain-lain

- [Jonnesway 6 Pcs Anti Slip Obeng Set - D71C106S](https://www.tokopedia.com/jonneswayofficial/jonnesway-6-pcs-anti-slip-obeng-set-d71c106s)
- [Feeler Gauge 32pcs Fuller Gauge Pengukur Setelan Klep SATA 09407](https://www.tokopedia.com/vinsteknik/feeler-gauge-32pcs-fuller-gauge-pengukur-setelan-klep-sata-09407)
- [Deli Vernier Caliper / Jangka Sorong Digital 6 inch Perkakas DL91150 - 0-150mm](https://www.tokopedia.com/delitoolsofficial/deli-vernier-caliper-jangka-sorong-digital-6-inch-perkakas-dl91150-0-150mm)
- [GRIP ON 97-100 Magnetic Tray - Mangkok Magnet Piring Treker Nampan](https://www.tokopedia.com/indahjayatools/grip-on-97-100-magnetic-tray-mangkok-magnet-piring-treker-nampan)
- [Tekiro Paket Tang Kombinasi-Potong-Lancip Set tekiro tang set 3 pcs](https://www.tokopedia.com/bigbossperkakas/tekiro-paket-tang-kombinasi-potong-lancip-set-tekiro-tang-set-3-pcs)
- Congkelan Ban / Cukit / Rim Crow Bar / Tyre Wrench
    - [Cungkit Ban Hitam 10" / Rim Crow Bar American Tool 8957770](https://www.tokopedia.com/american-tool/cungkit-ban-hitam-10-rim-crow-bar-american-tool-8957770)
    - [Tekiro Alat Congkelan 12 Inch Pembuka Pelepas Cukit Ban Roda Motor 12"](https://www.tokopedia.com/indojaya-tools/tekiro-alat-congkelan-12-inch-pembuka-pelepas-cukit-ban-roda-motor-12)
- [Scanner Motor Injeksi Universal MASTER ZEUS MST 100 PRO PLUS](https://www.tokopedia.com/tokoalatbengkel/scanner-motor-injeksi-universal-master-zeus-mst-100-pro-plus)

## Bahan dan Spare Part

### Umum

- [Diesel Purge GST52 Cleaner Injector Nozzle GST 52](https://www.tokopedia.com/indoutamaotopart/diesel-purge-gst52-cleaner-injector-nozzle-gst-52)
- [Liqui Moly Chain Spray White 400 ML - Pelumas Rantai 1591](https://www.tokopedia.com/liquimolyindo/liqui-moly-chain-spray-white-400-ml-pelumas-rantai-1591)
- [Rexco 70 Pelumas Gemuk Serba Guna (Degreaser) 500 Ml](https://www.tokopedia.com/tekiro-rexco/rexco-70-pelumas-gemuk-serba-guna-degreaser-500-ml)
- [REXCO PAKET GILA MOTOR](https://www.tokopedia.com/tekiro-rexco/rexco-paket-gila-motor-free-rain-cover-bag-rexco)
    - Rexco 25
    - Rexco 50
    - Rexco 75
- [Bar Tape 3T Prendo Speed Black Hitam](https://www.tokopedia.com/charliebike/bar-tape-3t-prendo-speed-black-hitam)
- [Kit Motor Chain Lube Aerosol 110mL (PULAU JAWA ONLY)](https://www.tokopedia.com/scjohnson-son/kit-motor-chain-lube-aerosol-110ml-pulau-jawa-only)
- [Oli Rantai TOP 1 Synthetic Chain Lube | 400 ml](https://www.tokopedia.com/top1store/oli-rantai-top-1-synthetic-chain-lube-400-ml)
- [STP Brake & Parts Cleaner Cairan Pembersih Rem Mobil Motor Sepeda ABS](https://www.tokopedia.com/stp/stp-brake-parts-cleaner-cairan-pembersih-rem-mobil-motor-sepeda-abs)
- [Air Radiator Mobil 5 Liter Powerplus / Radiator Coolant Power Plus](https://www.tokopedia.com/powerplus/air-radiator-mobil-5-liter-powerplus-radiator-coolant-power-plus)
- [Rac. Roller chain TDR - rantai tipis balap motor tdr 415ER - 130L gold](https://www.tokopedia.com/rajapartracing/rac-roller-chain-tdr-rantai-tipis-balap-motor-tdr-415er-130l-gold)

### Honda Revo

#### Oli

- [Oli Motor Evalube PRO Synthetic 10W-30 (1L)](https://www.tokopedia.com/evalube/oli-motor-evalube-pro-synthetic-10w-30-1l)
- [Oli Motor TOP 1 SMO EVOLUTION MC SAE 10W-30 | 1 Liter](https://www.tokopedia.com/top1store/oli-motor-top-1-smo-evolution-mc-sae-10w-30-1-liter)

#### Busi

- [Busi Motor DURATION SPARKPLUG 2W ULTRA IRIDIUM Seri BR8TUI](https://www.tokopedia.com/top1store/busi-motor-duration-sparkplug-2w-ultra-iridium-seri-br8tui)
- [Busi Motor BRISK Copper AR12C](https://www.tokopedia.com/briskbusiindo/busi-motor-brisk-copper-ar12c)

#### Ban Dalam

- Depan: [IRC TUBE 225.250-17 (70.90-17) Ban Dalam Motor](https://www.tokopedia.com/probanmotoparts/irc-tube-225-250-17-70-90-17-ban-dalam-motor)
- Belakang: [IRC TUBE 275-17 (80.90-17) Ban Dalam Motor](https://www.tokopedia.com/probanmotoparts/irc-tube-275-17-80-90-17-ban-dalam-motor)

### Yamaha FreeGo

#### Oli

##### Kombinasi

##### Oli Mesin

- [Liqui Moly Motorbike 4T 10W-40 Scooter 1 L - Oli Motor 1618](https://www.tokopedia.com/liquimolyindo/liqui-moly-motorbike-4t-10w-40-scooter-1-l-oli-motor-1618)

##### Oli Gardan

- [Oli gear/oli gardan Yamaha 100ml ( dijual perDus isi 48 botol/Dus )](https://www.tokopedia.com/raja-oli/oli-gear-oli-gardan-yamaha-100ml-dijual-perdus-isi-48-botol-dus)

### Suzuki GSX-R150

#### Oli

- [Oli Motor Shell Advance Ultra 10W-40 (1L)](https://www.tokopedia.com/shellindonesia/oli-motor-shell-advance-ultra-10w-40-1l)

### Kawasaki ZX-25R

#### Oli

- [KAWASAKI GENUINE OIL ULTIMATE 4T 10W/40 FULL SYNTHETIC OLI PELUMAS [1 LITER]](https://www.tokopedia.com/autobuddys/kawasaki-ultimate-4t-10w-40-full-synthetic-oli-pelumas-1-liter)
- [Liqui Moly Motorbike 4T 10W-40 Street 1L - Oli Motor 1521](https://www.tokopedia.com/liquimolyindo/liqui-moly-motorbike-4t-10w-40-street-1l-oli-motor-1521)
- [oli vent vert kawasaki elf 10w-50](https://www.tokopedia.com/kawansakti/oli-vent-vert-kawasaki-elf)

## Referensi Lain

- [Ganti oli dan radiator motor](https://gitlab.com/iamdejan/tutorial-menyetir-kendaraan#perawatan)
